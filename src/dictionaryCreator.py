## This script is used to create a json file containing the callpath.
## It uses the same structure as the caliper json files already supported by Hatchet

import tree
import numpy as np

def createNodeList(tree):
    functionNodes = tree.nodes
    nodeList = []
    for node in functionNodes:
        nodeDict = {}
        if (node.parent != None):
            nodeDict["column"] = "path"
            nodeDict["label"] = node.name
            nodeDict["parent"] = tree.search(node.parent.name)
        else:
            nodeDict["column"] = "path"
            nodeDict["label"] = node.name
        nodeList.append(nodeDict)
    return nodeList

def createMetricList(tree):
    functionNodes = tree.nodes
    nodeList = []

    ## The first entry of the json array is independent of program
    nodeMetrics = []
    nodeMetrics.append(int(9))
    nodeMetrics.append(int(0))
    nodeMetrics.append(9)
    nodeMetrics.append(None)
    nodeList.append(nodeMetrics)
    
    path = 0


    # For every node in tree we create a list of associated metrics
    for node in functionNodes:
        nodeMetrics = []
        nodeMetrics.append(float(node.data["Duration (ns)"])/1000000000)
        nodeMetrics.append(int(0))
        nodeMetrics.append(float(node.data["DurNonChild (ns)"])/1000000000)
        nodeMetrics.append(path)
        nodeList.append(nodeMetrics)
        path += 1
    return nodeList

def createGPUMetricList(tree, metrics, numColumns):
    functionNodes = tree.nodes
    nodeList = []

    ## The first entry of the json array is independent of program
    nodeMetrics = [0]*numColumns
    nodeMetrics[0] = int(9)
    nodeMetrics[1] = int(0)
    nodeMetrics[2] = int(9)
    nodeMetrics[3] = None
    nodeMetrics[4] = False
    nodeList.append(nodeMetrics)
    
    path = 0


    # For every node in tree we create a list of associated metrics
    for node in functionNodes:
        nodeMetrics = [np.nan]*numColumns
        nodeMetrics[0] = float(node.data["Duration (ns)"])/1000000000
        nodeMetrics[1] = (int(node.data("RankID")))
        nodeMetrics[2] = (float(node.data["DurNonChild (ns)"])/1000000000)
        nodeMetrics[3] = path
        if "isGPUKernel" in node.data:
            nodeMetrics[4] =  node.data["isGPUKernel"]
            counter = 5
            for value in metrics:
                #print("{0} = {1}".format(value, node.data[value]))
                nodeMetrics[counter] = float(node.data[value])
                counter+=1
        else:
            nodeMetrics[4] = False
        nodeList.append(nodeMetrics)
        path += 1
    return nodeList




def createCaliLikeDict(tree):
    print("CreateCaliLikeDict metrics")
    nsysDict = {}
    nsysDict["data"] = createMetricList(tree)
    nsysDict["columns"] = ["inclusive#sum#time.duration","mpi.rank","sum#time.duration","path"]
    nsysDict["column_metadata"] = [{"is_value": True},{"is_value": True},{"is_value": True},{"is_value": False}]
    nsysDict["nodes"] = createNodeList(tree)
    return nsysDict





def createCaliLikeDictGPU(tree, metrics):
    print("CreateCaliLikeDictGPU metrics")    
    nsysDict = {}
    nsysDict["columns"] = ["inclusive#sum#time.duration","mpi.rank","sum#time.duration","path","isGPUKernel"]
    nsysDict["columns"].extend(metrics)
    nsysDict["column_metadata"] = [{"is_value": True},{"is_value": True},{"is_value": True},{"is_value": False},{"is_value": True}]
    nsysDict["column_metadata"].extend([{"is_value": True}]*len(metrics))
    nsysDict["nodes"] = createNodeList(tree)
    numColumns = len(nsysDict["columns"])
    nsysDict["data"] = createGPUMetricList(tree, metrics,numColumns)
    return nsysDict

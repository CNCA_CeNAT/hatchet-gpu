"""! @brief ncu_reader 

   This module is used to read in an nsight compute csv kernel report containing relevant metrics for 
   each application kernel. 
   When several kernel repetitions are performed, an aggregation method is used to report a single metric for the kernel. 
"""
from os import close
import sys
from csv import DictReader
import pandas as pd 


def extractKernelMetrics(ncuDataFrame):
    """!extractKernelMetrics function
        Function to obtain the different call paths present in the metrics. This is done by obtaining the unique 
        call stacks in the Callstack column of the ncuDataFrame
        @param ncuDataFrame  Dataframe containing GPU metrics for the different kernels in an application
        @return kernels Python list of unique kernel calling contexts
    """
    kernels = ncuDataFrame.Callstack.unique()
    return kernels
    


def ncuReader(filename):
    """!ncuReader function.
        Preprocessing function that does some dataframe cleaning. It drops the Domain column, renames the Callstack column, 
        cleans the callstack values and removes the metrics starting with 'device__attribute'
        @param filename  Path to NCU GPU metrics report
        @return ncuDataMetrics Preprocessed and clean GPU-kernel metrics dataframe. 
        @return kernels Python list of unique kernel calling contexts
    """
    ncuDataMetrics = pd.read_csv(filename)

    
    ncuDataMetrics = ncuDataMetrics.drop("Domain", axis=1) #Domain column is not gonna be used so dropping it
    ncuDataMetrics.rename(columns={'Range:PL_Type:PL_Value:CLR_Type:Color:Msg_Type:Msg':'Callstack'}, inplace=True) #Renaming callstack information column 
    filter_cpu = ncuDataMetrics['Kernel Name'].str.contains("_kernel_agent")
    ncuDataMetrics = ncuDataMetrics[~filter_cpu]
    ncuDataMetrics["Callstack"] = ncuDataMetrics["Callstack"].apply(lambda x: x.replace(":none:none:none:none:none:none","").replace('"',"").split()) #Modify callstack columns values to make them simple Python arrays 
    ncuDataMetrics["Callstack"] = ncuDataMetrics["Callstack"].apply(lambda x: tuple(x)) #Had to break line 41 and 42 in to two different steps cause it wasn't working properly in some cases
    filter = ncuDataMetrics['Metric Name'].str.contains("device__attribute")
    ncuDataMetrics = ncuDataMetrics[~filter]
    kernels = extractKernelMetrics(ncuDataMetrics) 
    return ncuDataMetrics, kernels
    
def addMetricsToCallPath(callpath, filename):
    """!addMetricsToCallPath function.
        Entry point for the nsight compute report processing. This function updates the callpath inplace to add the kernel-specific metrics
        to each kernel node in the callpath. 
        @param callpath  Resulting hierarchical call path obtained from the nsys nvtx report
        @param filename Input nsight compute csv report of kernel metrics
        @return metrics Python list of the different metrics we obtain for each kernel
    """
    df, kernels = ncuReader(filename) #Read and preprocess input NCU metrics file
    metrics = []
    
    for item in kernels: #For every different kernel in metrics dataframe we want to get a kernel-specific dataframe
        kernel_metrics = df.loc[df['Callstack'] == item][["Metric Name","Average", "Minimum", "Maximum"]]
        #print("Parent {0}: Child {1}".format(item[-2],item[-1]))
        parent = callpath.getNode(callpath.search(item[-2]))
        kernelNode = callpath.getNode(callpath.searchByParent(item[-1], parent.__repr__()))
    
        kernelNode.data["isGPUKernel"] = True  #Mark node as being a GPU kernel function
    
        metrics = kernel_metrics["Metric Name"].unique()
        for metric in metrics:
            kernelNode.data[metric] = kernel_metrics.loc[kernel_metrics['Metric Name'] == metric]["Average"].values[0]
    return metrics



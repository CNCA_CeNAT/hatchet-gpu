# Based on  Shreyak Chakraborty's Tree implementation:  
# https://gist.github.com/kernelshreyak/d209a694ea5b4c312ab601f4dd6d7aa4
# Modified by Diego Jimenez so that Nodes not only hold a name but are also able 
# to hold any kind of data

import random
import string
import pptree

class TreeNode(object):
    "Node of a Tree"
    def __init__(self,nodeData, name='root', children=None,parent=None):
        self.name = name
        self.data = nodeData
        self.parent=parent
        self.children = []
        if children is not None:
            for child in children:
                self.add_child(child)
        
    def __repr__(self):
        return self.name

    def is_root(self):
        if self.parent is None:
            return True
        else:
            return False
    def is_leaf(self):
        if len(self.children) == 0:
            return True
        else:
            return False

    def depth(self):    # Depth of current node
        if self.is_root():
            return 0
        else:
            return 1 + self.parent.depth()

    def add_child(self, node):
        node.parent=self
        assert isinstance(node, TreeNode)
        self.children.append(node)
    
    def getChildren(self):
        return self.children

    def disp(self):
        pptree.print_tree(self,'children','name', horizontal=False)



class Tree:
    """
    Tree implemenation as a collection of TreeNode objects
    """
    def __init__(self):
       self.root=None
       self.height=0
       self.nodes=[]

    def insert(self,node,parent):   # Insert a node into tree
        if parent is not None:
            parent.add_child(node)
        else:
            if self.root is None:
                self.root=node
        self.nodes.append(node)

    def search(self,data):  # Search and return index of Node in Tree
        index=-1
        for N in self.nodes:
            index+=1
            if N.name == data:
                break
        if index == len(self.nodes)-1:
            return -1  #node not found
        else:
            return index

    def searchByParent(self, data, parent):  # Search and return index of Node in Tree
        index=-1
        for N in self.nodes:
            index+=1
            if (N.name == data and N.parent.name == parent):
                break
        if index == len(self.nodes)-1:
            return -1  #node not found
        else:
            return index

    def getNode(self,id):
        return self.nodes[id]

    def getRoot(self):
        return self.root

    # Adapted from https://vallentin.dev/2016/11/29/pretty-print-tree
    def pprint_tree(self, node, file=None, _prefix="", _last=True):
        print (_prefix, "`- " if _last else "|- ", node.name, sep="", file=file)
        _prefix += "   " if _last else "|  "
        child_count = len(node.children)
        for i, child in enumerate(node.children):
            _last = i == (child_count - 1)
            self.pprint_tree(child, file, _prefix, _last)

    def printNodes(self):
        for node in self.nodes:
            print (node.data)

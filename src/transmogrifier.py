#!/usr/bin/python

"""! @package Transmogrifier 

    This application reads in an NSight Systems generated NVTX trace report
    and recreates the call path of the application. The application reads in a csv file contaning the NVTX trace and 
    outputs a .json file describing the call path tree and associated node information
"""

##
# @mainpage Transmogrifier
#
# @section description_main Description
#    This application reads in an NSight Systems generated NVTX trace report
#    and recreates the call path of the application. The application reads in a csv file contaning the NVTX trace and 
#    outputs a .json file describing the call path tree and associated node information
#
# @section notes_main Notes
# - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#    DEALINGS IN THE SOFTWARE.
    #
# Developed by Diego Jimenez Vargas.  All rights reserved.


from os import close
import sys
import argparse
from csv import DictReader
import stack 
import tree
import json
import pandas as pd
from dictionaryCreator import *
from ncuReader import *

def alreadyExists(callTree, node, parent):
    """! alreadyExists function. 
        Checks if the node is already part of the children of parent in the callTree.
        @param callTree Call graph under construction
        @param node Node to check if it already exists as a children of parent
        @param parent Parent node used to check children list
        @return  boolean If the name of the node already exists in the children list of the parent node, return true, else return false. 
    """
    names = [x.name for x in parent.getChildren()]
    if (node.name in names):
        return True
    else:
        return False

def createHierarchy(trace_list):
    """! createHierarchy function.
        @param filename  Input NVTX csv report filename to process
        @return  callStack Resulting call stack after processing all lines in the input file
        @return hierarchy Resulting hierarchical call graph 
    """
    hierarchies_list = []
    callstack_list = []
    print(len(trace_list))
    for item in trace_list:
        callStack = stack.Stack()
        nsys_trace = item
        hierarchy = tree.Tree()
        for i in range(0,len(nsys_trace)):
            if callStack.isEmpty():
                callStack.push(nsys_trace[i])
                root = tree.TreeNode(nsys_trace[i], name=nsys_trace[i]["Name"])
                hierarchy.insert(root,None)
            else:
                currentStartTime = int(nsys_trace[i]["Start (ns)"])
                previousEndTime = int(callStack.top()["End (ns)"])
                if (previousEndTime > currentStartTime):
                    node = tree.TreeNode(nsys_trace[i], name=nsys_trace[i]["Name"])
                    parent = hierarchy.getNode(hierarchy.search(callStack.top()["Name"]))
                    if (alreadyExists(hierarchy, node, parent)):
                        #print("Case 1")
                        function = hierarchy.getNode(hierarchy.searchByParent(node.name, parent.__repr__()))
                        value = int(function.data["Duration (ns)"]) 
                        function.data["Duration (ns)"] = value + int(node.data["Duration (ns)"])
                    else:
                        hierarchy.insert(node,parent)
                    callStack.push(nsys_trace[i])

                else:
                    # If the function on top of the stack has already finished then it should be popped before pushing the new one.
                    while(int(callStack.top()["End (ns)"])< int(nsys_trace[i]["Start (ns)"])):
                        callStack.pop()
                    node = tree.TreeNode(nsys_trace[i], name=nsys_trace[i]["Name"])
                    parent = hierarchy.getNode(hierarchy.search(callStack.top()["Name"]))
                    if (alreadyExists(hierarchy, node, parent)):
                        #print ("Case 2")
                        function = hierarchy.getNode(hierarchy.searchByParent(node.name, parent.__repr__()))
                        value = int(function.data["Duration (ns)"]) 
                        function.data["Duration (ns)"] = value + int(node.data["Duration (ns)"])
                    else:
                        hierarchy.insert(node,parent)              
                    callStack.push(nsys_trace[i])       
        hierarchies_list.append(hierarchy)
        callstack_list.append(callStack)
    return callstack_list, hierarchies_list

def main(argv):
    """!Main function.
        @param argv  Command line arguments are taken as input to the program
    """
    inputFile = ''
    ncuReport = ''
    parser = argparse.ArgumentParser()
    parser.add_argument("-t","--nsysTrace", help="Input nsys trace file")
    parser.add_argument("-m","--ncuMetrics", help="Input compute metrics file")
    args = parser.parse_args()
    inputFile = args.nsysTrace
    ncuReport = args.ncuMetrics   
    if(inputFile == None):
        print("An nsys trace report is need to execute the program")
        exit()

    print ('Input file is ', inputFile)
    print ('NSight Compute report is ',ncuReport)

    
    trace_data = pd.read_csv(inputFile)
    rank_ids, processes = pd.factorize(trace_data["PID"],sort=True)
    processes = list(processes)
    trace_data["RankID"] = rank_ids
    print(processes)
    
    
    process_trace = []
    
    
    print("Subsetting dataframe")
    for p in processes:
        p_df = trace_data.loc[trace_data["PID"]==p]
        p_dict = p_df.to_dict('records')
        process_trace.append(list(p_dict))
    
    callStack_list, callgraph_list =  createHierarchy(process_trace)

    
    #callStack, callgraph = createHierarchy(inputFile)
    for item in callgraph_list:
        root = item.getRoot()
        print(root.data)
        print("----------------------------------------------------------")
        item.pprint_tree(root)
    
    #########################################
    ####### OLD CODE #######################
    #print("Tree nodes as list")
    #callgraph.printNodes()

    #hierarchyDict = build_tree(callgraph.nodes)
    #print(hierarchyDict)
    #callgraph.root.disp()
    #print(callStack.printStack())

    #########################################
    
    for idx,callgraph in enumerate(callgraph_list):
        if (ncuReport == None):
            print("No GPU metrics will be loaded")
            nsysDict = createCaliLikeDict(callgraph)
            #print(nsysDict)
            filename = "output_files/hierarchy_{rank}.json".format(rank=idx)

            with open(filename,"w+") as f:
                json.dump(nsysDict,f, indent=1)
        else:
            gpu_metrics = addMetricsToCallPath(callgraph, ncuReport)
            #print(gpu_metrics)
            nsysDict = createCaliLikeDictGPU(callgraph, gpu_metrics)
            #print(nsysDict)
            with open("output_files/hierarchy.json","w+") as f:
                json.dump(nsysDict,f, indent=1)
     
    print("--------------Processing complete, hierarchy.json in output_files file created--------------")
if __name__ == "__main__":
    main(sys.argv[1:])
